---
layout: markdown_page
title: "Processing Payroll"
---

## On this page
{:.no_toc}

- TOC
{:toc}



## Processing Payroll
<a name="payroll"></a>

### GitLab Inc. Payroll Procedures
<a name="payroll-inc"></a>

1. PeoplesOps will notify Payroll when new hires are added in BambooHR and I9 verification is completed
1. Payroll admin adds new team member into ADP via Payroll system only template
1. Email new hires with the ADP registration guide and ask them to update tax withholding and add direct deposit
1. All additional payment requests must received by the payroll changes due date.  The payroll schedule in ADP under Quick Links.
1. Sr. PeopleOps Analyst updates salary, department, job title, and managers in ADP WFN before or by the due date
1. Log into Betterment to run the current deferral rates report on the payroll processing date (5 days before check date)
1. Update new deferral rates to team member's record in ADP WFN under deduction
1. Lumity will send a benefits election change report by the 1st and the 15th of each month to payroll
1. Update/Add these benefits elections in ADP (be sure to enter employer benefits premium)
1. Log into ADP WFN via Admin access
1. Start a new payroll cycle under Process and Payroll
1. Review the week # and check date
1. All salary team members are set up with autopaid for 86.67 hours per check date.
1. Create a batch for hourly employees, LOA, new hires, and/or termination
1. Create a batch for one time payment (referal bonus, discretion bonus, commission, SDR bonus, and quarterly bonus) as check number #2 or 3 with Bonus frequency
1. Create a batch for benefits corrections as needed
1. Be sure to enter W under Special action column in the one time payment batch to cancel the calculation of GTL
1. Send payroll to preview
1. Review the preview register, make corrections as needed, and resend to preview
1. Accept payroll 

All hourly time sheets are kept on the Google Drive and shared with Finance. Each employee will populate the time sheet before the end of the pay cycle.

#### One time payment

1. Create a batch and name it accordingly
1. Selecte the Bonus paydata grid
1. Add employee
1. Enter the earning type and amount
1. Enter B pay frequency
1. Enter #2, or 3 under pay #
1. Enter W under Special Action

### Lumity Payroll Processes for GitLab Inc.

#### Payroll Process

Payroll files will be provided to GitLab by Lumity to the People Operations Analyst and the Payroll and Payments Lead. Lumity will send a “Diff” payroll file and ADP Upload file on each Payroll Send Date. GitLab will review the payroll schedule and notify Lumity if send dates need to be changed. The ADP Upload file will only contain standard per pay period deduction amounts. GitLab will add one time catch-up and/or credits through ADP using the DIFF file.

#### Funding Process

Lumity will manage and fund all Discovery accounts. Employee and Employer funding will be taken from the payroll report provided to Lumity by GitLab.

* H.S.A
  * Employer contributions will be funded each payroll ($50 per pay)
  * Missed ER contribution will not have a catch up (Employee enrolls late…Lumity will only fund ER contribution on the upcoming pay period. Any missed employer contributions will be disregarded)
  * Discovery will debit GitLab Bank account on each funding date
  * Max out is allowed
* FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Dependent Care FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Limited FSA
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds
* Commuter
  * Employee payroll deduction will occur on the last payroll of the month and funded on the 1st of the following month
  * Funds will debit from the Discovery reserve account once the employee submits a claim
  * Discovery will contact GitLab if the reserve is low on funds

### GitLab BV Pay Slip Distribution Process
<a name="payroll-bv"></a>

All GitLab BV employees receive their payslips within their personal portal of Savvy.
They can login and download their payslip to their computer if needed.

### UK, Belgium, & India Monthly Payroll Process

Payroll cut off for sending changes is usually the middle of the month (15th-17th), in addition, expenses for the UK should also be submitted at this time as they are paid via the payroll.The payroll provider will send a report for approval to the People Operations email address and copy in the Financial Controller. The Financial Controller will approve the payroll by sending a confirmation email back to the payroll provider. Once processed the payslips are sent electronically directly to the team members for them to access via a password protected system.

### SafeGuard Payroll Process

Payroll cut off for sending changes is at the beginning of each month. The process for submitting changes is as follows:

1. People Operations will confirm if there are any expenses or commission payments to process from Finance
2. People Operations will make a copy of the [SafeGuard Payroll Changes](https://docs.google.com/spreadsheets/d/1VkRI3GuRpu4kMI9zQ_016TZiJMKWIZUSmRj_Bt6cYkM/edit#gid=1648114438) sheet and add the changes
3. The change sheet for that month should then be moved to the applicable country payroll changes folder
4. Download a copy of the sheet and email it to SafeGuard. Contact details are located in the people ops 1password vault => Entity & Co-Employer HR Contacts


## Commission Payment Process
<a name="commission"></a>

1. Each sales person will receive their own calculation template.
1. Salesperson is to complete their monthly template four days (payroll will send reminder) prior to first payroll of the month. Upon completion, salesperson will ping a manager for review and approval.
1. Approving manager will ping accounting upon approval.
1. Accounting will review and reconcile paid vs unpaid invoices.
1. Accounting will note in calculation template the amounts to be paid in commission.
1. Accounting will ping payroll that commission calculation is complete.
1. Payroll will enter commission into ADP
